package com.sda.dp.game.view;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.interfaces.IFireListener;
import com.sda.dp.game.model.*;
import com.sda.dp.game.model.HeartObject;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainPanel extends JPanel implements IFireListener {
    private final Color BACKGROUND_COLOR = Color.GRAY;
    private final int width;
    private final int height;
    private final int MAX_MONSTER_SHOTS = 2;
    private HeartObject lifeHeart;

    private List<AbstractGameObject> objectList;
    private List<AbstractGameObject> fireList;
    private List<AbstractGameObject> heartList;
    private List<AbstractGameObject> monsterFireList;
    private List<Integer> numberOfMonstersKilled;

    private GameHero hero;
    private GameText gameOverText;
    private GameText numberOfPointsText;
//    private HeartObject heartObject;

    public MainPanel(int width, int height) {
        super();
        this.width = width;
        this.height = height;

        lifeHeart = new HeartObject();
        // create stuff
        objectList = new ArrayList<>();
        fireList = new ArrayList<>();
        heartList = new ArrayList<>();
        monsterFireList = new ArrayList<>();
        numberOfMonstersKilled = new ArrayList<>();

        gameOverText = new GameText(width/2, height/2, "Game Over \n You Lost!");
        numberOfPointsText = new GameText(width-50,height-20, ""+ numberOfMonstersKilled.size());
        // set stuff
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
        setLayout(null);

        Dispatcher.instance.registerObject(this);
    }

    public void addCharacterIntoGame(AbstractGameObject gameObject) {
        objectList.add(gameObject);
    }

    public void addHeartsIntoGame(AbstractGameObject gameHeart) {
        heartList.add(gameHeart);
    }

    public void addFireIntoGame(AbstractGameObject gameObject) {
        fireList.add(gameObject);
    }

    private void addMonsterFireIntoGame(Fire fire) {
        monsterFireList.add(fire);
    }

    public void setGameHero(GameHero hero) {
        this.hero = hero;
    }

//    public void setGameHeart(HeartObject heartObject) {
//        this.heartObject = heartObject;
//    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(BACKGROUND_COLOR);
        g2d.fillRect(0, 0, width, height);
        if (hero.getHealth() > 0) {
            paintWorld(g2d);
        } else {
            paintGameOver(g2d);
        }
    }

    private void paintGameOver(Graphics2D g2d) {
        gameOverText.paint(g2d);
    }

    private void paintWorld(Graphics2D g2d) {
        for (int i = hero.getHealth(); i > 0; i--) {
            lifeHeart.paint(g2d, i);
        }

        g2d.setColor(Color.white);
        //drawing character
        AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
        objectsToPaint = objectList.toArray(objectsToPaint);

        for (AbstractGameObject objectToPaint : objectsToPaint) {
            objectToPaint.paint(g2d);
        }

        //drawing shots
        AbstractGameObject[] firesToPaint = new AbstractGameObject[fireList.size()];
        firesToPaint = fireList.toArray(firesToPaint);

        for (AbstractGameObject fireToPaint : firesToPaint) {
            fireToPaint.paint(g2d);
        }

        //drawing monster shots
        AbstractGameObject[] monsterFires = new AbstractGameObject[monsterFireList.size()];
        monsterFires = monsterFireList.toArray(monsterFires);

        for (AbstractGameObject monsterFire : monsterFires) {
            monsterFire.paint(g2d);
        }

        hero.paint(g2d);
    }

    public void move(double move) {
        Random random = new Random();
        if (move > 0) {
            AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
            objectsToPaint = objectList.toArray(objectsToPaint);
            for (AbstractGameObject objectToPaint : objectsToPaint) {
                if (objectToPaint != null) {
                    objectToPaint.move(move);
                }
                if (monsterFireList.size() < MAX_MONSTER_SHOTS) {
                    if (random.nextInt(1000) >= 995)
                        //generate monster shot
                        addMonsterFire(objectToPaint.getPositionX(), objectToPaint.getPositionY());
                }
            }


            AbstractGameObject[] firestToMove = new AbstractGameObject[fireList.size()];
            firestToMove = fireList.toArray(firestToMove);
            for (AbstractGameObject fireToPaint : firestToMove) {
                if (fireToPaint != null) {
                    fireToPaint.move(move);
                }
            }

            AbstractGameObject[] monsterFirestToMove = new AbstractGameObject[monsterFireList.size()];
            monsterFirestToMove = monsterFireList.toArray(firestToMove);
            for (AbstractGameObject monsterFireToPaint : monsterFirestToMove) {
                if (monsterFireToPaint != null) {
                    monsterFireToPaint.move(move);
                    if (monsterFireToPaint.getPositionY() > height) {
                        monsterFireList.remove(monsterFireToPaint);

                    }
                }
            }
            hero.move(move);
        }
    }

    public void checkCollisions() {
        AbstractGameObject[] objectsToMove = new AbstractGameObject[objectList.size()];
        objectsToMove = objectList.toArray(objectsToMove);
        AbstractGameObject[] firesToMove = new AbstractGameObject[fireList.size()];
        firesToMove = fireList.toArray(firesToMove);
        AbstractGameObject[] monsterFireMoves = new AbstractGameObject[monsterFireList.size()];
        monsterFireMoves = monsterFireList.toArray(monsterFireMoves);
        // ConcurrentModificationException
        for (AbstractGameObject strzal : firesToMove) {
            if (!strzal.isToBeRemoved())
                for (AbstractGameObject postac : objectsToMove) {
                    if (postac.checkCollision(strzal) && !strzal.isToBeRemoved()) {
                        strzal.setToBeRemoved(true);
                        postac.hit();

                        if (postac.isToBeRemoved()){
                            objectList.remove(postac);
                            numberOfMonstersKilled.add(1);
                        }
                        fireList.remove(strzal);
                    }
                }
        }
        for (AbstractGameObject monsterShoots : monsterFireMoves) {
            if (hero.checkCollision(monsterShoots)) {
                hero.hit();
                monsterFireList.remove(monsterShoots);
            }
        }
    }

    public void addFire(int posX, int posY, double speed, boolean up) {
        addFireIntoGame(new Fire(posX, posY, speed, up));
    }


    public void addMonsterFire(int posX, int posY) {
        addMonsterFireIntoGame(new Fire(posX, posY, 5, false));
    }
}