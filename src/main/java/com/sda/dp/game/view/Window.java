package com.sda.dp.game.view;

import com.sda.dp.game.model.*;
import jdk.internal.org.objectweb.asm.tree.MultiANewArrayInsnNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Window extends JFrame {

    private ExecutorService repaintThread = Executors.newSingleThreadExecutor();

    // rozmiar okna
    private static final int SIZE_WIDTH = 800;
    private static final int SIZE_HEIGHT = 600;
    private static final double FPS = 90;
    private boolean stopGame = false;
    private MainPanel mainPanel;
    private Dimension windowSize;

    private GameHero hero;

    private long timeRepaint = 0L;
    private double timeBetween = 1000 / FPS;

    public Window() throws HeadlessException {
        super();

        // create stuff
        this.mainPanel = new MainPanel(SIZE_WIDTH, SIZE_HEIGHT);
        this.windowSize = new Dimension(SIZE_WIDTH, SIZE_HEIGHT);

        addObjects();
//         set stuff
        setLayout(new BorderLayout());
        getContentPane().add(mainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(this.windowSize);
        setPreferredSize(this.windowSize);

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
//                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
//                    hero.moveDown();
//                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
//                    hero.moveUp();
//                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    hero.moveLeft();
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    hero.moveRight();
                } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    if (!stopGame) {
                        hero.fire();
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_P) {
                    stopGame = !stopGame;
                }
                super.keyPressed(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    hero.stopDown();
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    hero.stopUp();
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    hero.stopLeft();
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    hero.stopRight();
                }
                super.keyReleased(e);
            }
        });

        repaintThread.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    long timeStart, timeEnd;
                    while (true) {
                        timeStart = System.currentTimeMillis();
                        repaint();
                        timeEnd = System.currentTimeMillis();
                        long diff = (long) (timeBetween - (timeEnd - timeStart));
                        if (diff > 0) {
                            try {
                                Thread.sleep(diff);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        mainPanel.checkCollisions();
                        if (stopGame) {
                        } else if (!stopGame) {
                            mainPanel.move(diff / 10.0);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        pack();
    }


    public void addObjects() {
        hero = new GameHero(300, 500);
        mainPanel.setGameHero(hero);

        loadLevel(1);
    }

    private void loadLevel(int level) {
        try(BufferedReader reader= new BufferedReader(new FileReader("levels/level_"+level+".silf"))){
                String line = null;
                while((line = reader.readLine()) != null){
                String[] splits = line.split(" ");
                int poX = Integer.parseInt(splits[0]);
                int poY = Integer.parseInt(splits[1]);
                int dur = Integer.parseInt(splits[2]);
                int range = Integer.parseInt(splits[3]);
                mainPanel.addCharacterIntoGame(new EnemyShipObject(poX,poY,dur,range));
                }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void usuwanieZyc() {
        for (int i = 3; i > 0; i--) {
            try {
                Thread.sleep(2000);
                hero.setHealth(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Deprecated
    private void printHealth(GameHero hero) {
        int hp = hero.getHealth();
        for (int i = 300; i > 0; i -= 100) {
            mainPanel.addHeartsIntoGame(new HeartObject());
        }
    }

}
