package com.sda.dp.game.dispatcher;

import com.sda.dp.game.interfaces.IFireListener;

import java.util.List;

public class HeroFiredEvent implements IEvent {
    private int x;
    private int y;

    public HeroFiredEvent(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void run() {
        List<IFireListener> listeners = Dispatcher.instance.getAllObjectsImplementingInterface(IFireListener.class);
        for (IFireListener listener: listeners) {
            listener.addFire(x,y,7,true);
        }
    }
}
