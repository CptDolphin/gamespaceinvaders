package com.sda.dp.game.model;

import java.awt.*;

public class GameText extends AbstractGameObject {

    private String text;

    public GameText(int x, int y, String text) {
        super(new Point(x, y));
        this.text = text;
    }

    @Override
    public void paint(Graphics2D g2d) {
        Color currentColor = g2d.getColor();
        g2d.setColor(Color.BLUE);
        g2d.drawString(text, this.position.x, this.position.y );
        drawCenteredString(g2d, text, new Rectangle(800, 600), new Font("Arial",Font.BOLD,50));
        // set last color
        g2d.setColor(currentColor);
    }
    public void drawCenteredString(Graphics2D g, String text, Rectangle rect, Font font) {
        FontMetrics metrics = g.getFontMetrics(font);
        int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        g.setFont(font);
        g.drawString(text, x, y);
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public void hit() {

    }
}
