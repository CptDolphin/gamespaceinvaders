package com.sda.dp.game.model;

import java.awt.*;

public class CircleObject extends AbstractGameObject{

    public CircleObject(int x, int y) {
        super(new Point(x, y));
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        Color currentColor = g2d.getColor();

        g2d.setColor(Color.RED);
        g2d.fillOval(position.x, position.y, 50, 50);
        g2d.setColor(currentColor);
    }

    @Override
    public int getHeight() {
        return 50;
    }

    @Override
    public int getWidth() {
        return 50;
    }

    @Override
    public void hit() {

    }
}
