package com.sda.dp.game.model;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.dispatcher.HeroFiredEvent;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameHero extends AbstractGameObject {

    private BufferedImage image;
    private int health = 3;

    public GameHero(int x, int y) {
        super(new Point(x, y));
        try {
            image = ImageIO.read(new File("src/main/resources/hero.png"));
        } catch (IOException ex) {
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }

    public void fire() {
        int positionX = image.getWidth() / 2 + position.x - 5;
        Dispatcher.instance.dispatch(new HeroFiredEvent(position.x, position.y));
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {
        health--;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
