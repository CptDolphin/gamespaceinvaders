package com.sda.dp.game.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class HeartObject extends AbstractGameObject {

    private BufferedImage image;

    public HeartObject() {
        try {
            image = ImageIO.read(new File("src/main/resources/heart.png"));
        } catch (IOException ex) {
        }
    }

    public void paint(Graphics2D g2d, int i) {
        position.x = i * getWidth() + i * 5 - getWidth();
        position.y = 0;
        paint(g2d);
    }

    public void paint(Graphics2D g2d) {
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {

    }
}
