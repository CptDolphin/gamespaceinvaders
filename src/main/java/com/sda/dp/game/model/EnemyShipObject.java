package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class EnemyShipObject extends AbstractGameObject {
    private int hp;
    private int range;
    private BufferedImage image;

    private Point initialPosiotion;

    public EnemyShipObject(int x, int y, int durability, int range) {
        super(new Point(x, y));
        this.initialPosiotion = new Point(x, y);
        this.hp = durability;
        this.range = range;
        try {
            image = ImageIO.read(new File("src/main/resources/monster.png"));
        } catch (IOException ex) {
            System.err.println("Couldn't load picutre");
        }
        hDir = HorizontalDirection.RIGHT;
    }

    @Override
    public void move(double step) {
        int differentX = position.x - initialPosiotion.x ;
        if (differentX > range) {
            hDir = HorizontalDirection.LEFT;
        } else if (differentX <= 0) {
            hDir = HorizontalDirection.RIGHT;
        }
        super.move(step);
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {
        hp--;
        if(hp==0){
            toBeRemoved = true;
        }
    }
}
